import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SantaRoComponent } from './santa-ro.component';

describe('SantaRoComponent', () => {
  let component: SantaRoComponent;
  let fixture: ComponentFixture<SantaRoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SantaRoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SantaRoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
