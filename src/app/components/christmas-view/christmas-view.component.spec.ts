import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChristmasViewComponent } from './christmas-view.component';

describe('ChristmasViewComponent', () => {
  let component: ChristmasViewComponent;
  let fixture: ComponentFixture<ChristmasViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChristmasViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChristmasViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
