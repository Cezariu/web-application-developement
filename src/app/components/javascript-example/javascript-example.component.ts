import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-javascript-example',
  templateUrl: './javascript-example.component.html',
  styleUrls: ['./javascript-example.component.css']
})
export class JavascriptExampleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public num1: number;
  public num2: number;
  public result: number;

  addition() {
    this.result = this.num1 + this.num2;
  }
  subtract() {
    this.result = this.num1 - this.num2;
  }
  multiply() {
    this.result = this.num1 * this.num2;
  }
  divide() {
    this.result = (this.num1) / (this.num2);
  }
}
