import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JavascriptExampleComponent } from './javascript-example.component';

describe('JavascriptExampleComponent', () => {
  let component: JavascriptExampleComponent;
  let fixture: ComponentFixture<JavascriptExampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JavascriptExampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JavascriptExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
