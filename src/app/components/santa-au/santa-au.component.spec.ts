import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SantaAuComponent } from './santa-au.component';

describe('SantaAuComponent', () => {
  let component: SantaAuComponent;
  let fixture: ComponentFixture<SantaAuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SantaAuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SantaAuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
