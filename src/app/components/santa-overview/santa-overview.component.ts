import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-santa-overview',
  templateUrl: './santa-overview.component.html',
  styleUrls: ['./santa-overview.component.css']
})
export class SantaOverviewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  openChSanta() {
    let modal = document.getElementById("ChSantaModal") as HTMLElement;
    modal.style.display = "block";
  }

  closeChSanta() {
    let modal = document.getElementById("ChSantaModal") as HTMLElement;
    modal.style.display = "none";
  }

  openAuSanta() {
    let modal = document.getElementById("AuSantaModal") as HTMLElement;
    modal.style.display = "block";
  }

  closeAuSanta() {
    let modal = document.getElementById("AuSantaModal") as HTMLElement;
    modal.style.display = "none";
  }

  openRoSanta() {
    let modal = document.getElementById("RoSantaModal") as HTMLElement;
    modal.style.display = "block";
  }

  closeRoSanta() {
    let modal = document.getElementById("RoSantaModal") as HTMLElement;
    modal.style.display = "none";
  }






}
