import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SantaOverviewComponent } from './santa-overview.component';

describe('SantaOverviewComponent', () => {
  let component: SantaOverviewComponent;
  let fixture: ComponentFixture<SantaOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SantaOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SantaOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
