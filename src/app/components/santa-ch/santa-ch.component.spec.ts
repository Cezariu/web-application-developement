import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SantaChComponent } from './santa-ch.component';

describe('SantaChComponent', () => {
  let component: SantaChComponent;
  let fixture: ComponentFixture<SantaChComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SantaChComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SantaChComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
