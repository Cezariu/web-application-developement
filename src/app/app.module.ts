import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FirstLabComponent } from './components/first-lab/first-lab.component';
import { CvComponent } from './components/cv/cv.component';
import { PostcardComponent } from './components/postcard/postcard.component';
import { JavascriptExampleComponent } from './components/javascript-example/javascript-example.component';
import { CalculatorComponent } from './components/calculator/calculator.component';
import { FormsModule } from '@angular/forms';
import { ChristmasViewComponent } from './components/christmas-view/christmas-view.component';
import { SantaOverviewComponent } from './components/santa-overview/santa-overview.component';
import { SantaRoComponent } from './components/santa-ro/santa-ro.component';
import { SantaChComponent } from './components/santa-ch/santa-ch.component';
import { SantaAuComponent } from './components/santa-au/santa-au.component';
import { GiftPageComponent } from './components/gift-page/gift-page.component';


@NgModule({
  declarations: [
    AppComponent,
    FirstLabComponent,
    CvComponent,
    PostcardComponent,
    JavascriptExampleComponent,
    CalculatorComponent,
    ChristmasViewComponent,
    SantaOverviewComponent,
    SantaRoComponent,
    SantaChComponent,
    SantaAuComponent,
    GiftPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
