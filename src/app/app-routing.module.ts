import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirstLabComponent } from './components/first-lab/first-lab.component';
import { CvComponent } from './components/cv/cv.component';
import { PostcardComponent } from './components/postcard/postcard.component';
import { JavascriptExampleComponent } from './components/javascript-example/javascript-example.component';
import { CalculatorComponent } from './components/calculator/calculator.component';
import { ChristmasViewComponent } from './components/christmas-view/christmas-view.component';
import { SantaOverviewComponent } from './components/santa-overview/santa-overview.component';
import { GiftPageComponent } from './components/gift-page/gift-page.component';


const routes: Routes = [
  { path: "firstlab", component: FirstLabComponent },
  { path: "cv", component: CvComponent },
  { path: "postcard", component: PostcardComponent },
  { path: "js", component: JavascriptExampleComponent },
  { path: "calculator", component: CalculatorComponent },
  { path: "christmas", component: ChristmasViewComponent},
  { path: "santa", component: SantaOverviewComponent},
  { path: "gifts", component: GiftPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
